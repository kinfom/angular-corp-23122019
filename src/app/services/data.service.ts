import { apiurl } from "./../../assets/apiUrl";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class DataService {
  constructor(private http: HttpClient) {
    this.getAllUsers();
  }

  users: any;
  getAllUsers() {
    return this.http.get(apiurl + "users");
    //   this.http
    //     .get("https://jsonplaceholder.typicode.com/users")
    //     .subscribe(data => {
    //       this.users = data;
    //       console.log(this.users);
    //     });
    // }
  }
  getSingleUser(id) {
    return this.http.get("https://jsonplaceholder.typicode.com/users/" + id);
  }

  insertPost(post) {
    return this.http.post("https://jsonplaceholder.typicode.com/posts", post);
  }
}
