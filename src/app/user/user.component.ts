import { DataService } from "./../services/data.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"]
})
export class UserComponent implements OnInit {
  constructor(private route: ActivatedRoute, private data: DataService) {}

  id: any;
  user: any;
  name: string;
  email: string;
  username: string;
  isError: boolean;
  errMsg: string = "";
  ngOnInit() {
    this.route.params.subscribe(param => {
      this.id = param.id;
      this.data.getSingleUser(this.id).subscribe(user => {
        this.user = user;
        this.name = this.user.name;
        this.email = this.user.email;
        this.username = this.user.username;
      });
    });
  }
  submitData(data) {
    if (data.name.length < 3) {
      this.isError = true;
      this.errMsg += "Name should be minimum 3 letters";
      return false;
    }
    if (data.username.length < 3) {
      this.isError = true;
      this.errMsg += "Username should be minimum 3 letters";
      return false;
    }

    console.log(data);
    this.data.insertPost(data).subscribe(data1 => console.log(data1));
  }
}
