import { Directive, ElementRef } from "@angular/core";

@Directive({
  selector: "[appDescHighlight]"
})
export class DescHighlightDirective {
  constructor(ele: ElementRef) {
    console.log(ele);
    ele.nativeElement.innerText = "Product Description: ";
    ele.nativeElement.style.color = "black";
  }
}
