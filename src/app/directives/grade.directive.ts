import { ElementRef, AfterViewInit } from "@angular/core";
import { Directive } from "@angular/core";

@Directive({
  selector: "[appGrade]"
})
export class GradeDirective implements AfterViewInit {
  constructor(private ele: ElementRef) {
    let marks = ele.nativeElement.innerHTML;
    // console.log(ele);
  }
  ngAfterViewInit() {
    console.log(this.ele.nativeElement.innerText);
    let grade = this.ele.nativeElement.innerText;
    console.log(grade);
    if (grade >= 80) {
      this.ele.nativeElement.style.backgroundColor = "green";
      this.ele.nativeElement.innerText = "A";
    } else if (grade > 60 && grade < 80) {
      this.ele.nativeElement.style.backgroundColor = "orange";
      this.ele.nativeElement.innerText = "B";
    } else {
      this.ele.nativeElement.style.backgroundColor = "red";
      this.ele.nativeElement.innerText = "C";
    }
  }
}
