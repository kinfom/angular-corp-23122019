import { Component, OnInit } from "@angular/core";
import { NameService } from "./../services/name.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  name: string;
  showDescription: boolean;
  age = 20;
  ngifdemo = true;

  constructor(private nameService: NameService) {} // instatiation

  ngOnInit() {
    // initialization
    this.name = "Krishna";
    this.nameService.setName(this.name); // call the service and set the name in service
    this.showDescription = true;
  }
  changeText() {
    // alert("hi" + this.name);
    // this.name = document.getElementById("name").value;
    this.nameService.setName(this.name); // call the service and set the name in service
  }
  clickHandler() {
    this.showDescription = !this.showDescription;
    // if (this.showDescription == true) {
    //   this.showDescription = false;
    // } else {
    //   this.showDescription = true;
    // }
  }
}
