import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-grade",
  templateUrl: "./grade.component.html",
  styleUrls: ["./grade.component.css"]
})
export class GradeComponent implements OnInit {
  constructor() {}
  @Input() grade;
  ngOnInit() {}
}
