import { LazyModule } from "./lazy/lazy.module";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { HomeComponent } from "./home/home.component";
import { ProductsComponent } from "./products/products.component";
import { ProductTitleComponent } from "./products/product-title/product-title.component";
import { DescHighlightDirective } from "./directives/desc-highlight.directive";
import { DiscountPipe } from "./pipes/discount.pipe";
import { MarksComponent } from "./marks/marks.component";
import { GradePipe } from "./pipes/grade.pipe";
import { GradeDirective } from "./directives/grade.directive";
import { GradeComponent } from "./grade/grade.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { HttpClientModule } from "@angular/common/http";
import { UsersComponent } from "./users/users.component";
import { DataFilterPipe } from "./data-filter.pipe";
import { UserComponent } from "./user/user.component";
import { ArticleFormComponent } from "./article-form/article-form.component";
import { AnimationComponent } from "./animation/animation.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ProductsComponent,
    ProductTitleComponent,
    DescHighlightDirective,
    DiscountPipe,
    MarksComponent,
    GradePipe,
    GradeDirective,
    GradeComponent,
    PageNotFoundComponent,
    UsersComponent,
    DataFilterPipe,
    UserComponent,
    ArticleFormComponent,
    AnimationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    LazyModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
