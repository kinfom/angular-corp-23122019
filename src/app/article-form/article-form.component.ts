import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-article-form",
  templateUrl: "./article-form.component.html",
  styleUrls: ["./article-form.component.css"]
})
export class ArticleFormComponent implements OnInit {
  constructor(private fb: FormBuilder) {}

  articleForm: FormGroup;
  submitted = false;

  ngOnInit() {
    this.articleForm = this.fb.group({
      title: ["test", [Validators.required, Validators.minLength(5)]],
      user: ["", [Validators.required, Validators.minLength(5)]],
      email: ["", [Validators.required, Validators.email]],
      article: ["", [Validators.required, Validators.minLength(5)]]
    });
  }

  get f() {
    // console.log(this.articleForm.controls);
    return this.articleForm.controls;
  }
  submitHandler() {
    // this.f;
    this.submitted = true;
    if (this.articleForm.invalid) {
      console.log("error in form");
      return;
    }
    console.log(this.articleForm.value);
  }
}
