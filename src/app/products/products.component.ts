import { NameService } from "./../services/name.service";
import { Component, OnInit } from "@angular/core";
import { DATA } from "./../../assets/products";

@Component({
  selector: "app-products",
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.css"]
})
export class ProductsComponent implements OnInit {
  data: any;
  container_background = "#eee";
  cols = 4;

  constructor(private nameService: NameService) {}

  ngOnInit() {
    // http call -> observaable which products array
    this.data = DATA;
    this.nameService.setName("manju");
    // console.log(this.products);
  }
}
