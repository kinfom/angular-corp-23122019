import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  // template: "<h2>Hello World</h2>",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  mytitle = "my-great-app";
}
