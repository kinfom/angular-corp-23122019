import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "grade"
})
export class GradePipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    if (value >= 80) {
      return "A";
    } else if (value < 80 && value >= 60) {
      return "B";
    } else {
      return "C";
    }
    return null;
  }
}
