import { Component, OnInit } from "@angular/core";
import {
  trigger,
  transition,
  state,
  style,
  animate
} from "@angular/animations";

@Component({
  selector: "app-animation",
  templateUrl: "./animation.component.html",
  styleUrls: ["./animation.component.css"],
  animations: [
    trigger("popOverState", [
      state("show", style([{ opacity: 1 }])),
      state("hide", style([{ opacity: 0 }])),
      transition("show => hide", animate("600ms ease-out")),
      transition("hide => show", animate("1000ms ease-in"))
    ])
  ]
})
export class AnimationComponent implements OnInit {
  constructor() {}

  show = false;
  ngOnInit() {}

  get stateName() {
    return this.show ? "show" : "hide";
  }

  toggle() {
    this.show = !this.show;
  }
}
