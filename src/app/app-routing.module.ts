import { AnimationComponent } from "./animation/animation.component";
import { ThreeComponent } from "./lazy/three/three.component";
import { TwoComponent } from "./lazy/two/two.component";
import { OneComponent } from "./lazy/one/one.component";
import { ArticleFormComponent } from "./article-form/article-form.component";
import { UserComponent } from "./user/user.component";
import { UsersComponent } from "./users/users.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { MarksComponent } from "./marks/marks.component";
import { ProductsComponent } from "./products/products.component";
import { HomeComponent } from "./home/home.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", redirectTo: "/home", pathMatch: "full" },
  { path: "home", component: HomeComponent },
  { path: "products", component: ProductsComponent },
  { path: "users", component: UsersComponent },
  { path: "user/:id", component: UserComponent },
  { path: "marks", component: MarksComponent },
  { path: "animation", component: AnimationComponent },
  { path: "article", component: ArticleFormComponent },
  {
    path: "lazy",
    loadChildren: "./lazy/lazy.module#LazyModule"
  },

  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
