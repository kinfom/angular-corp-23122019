import { NameService } from "./../services/name.service";
import { STUDENTS } from "./../../assets/students";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-marks",
  templateUrl: "./marks.component.html",
  styleUrls: ["./marks.component.css"]
})
export class MarksComponent implements OnInit {
  constructor(
    private studentNameService: NameService,
    private router: Router
  ) {}

  students: any[];
  name: string;
  ngOnInit() {
    this.name = this.studentNameService.name;
    if (this.name.length == 0) {
      // navigate to home
      setTimeout(() => this.router.navigate(["/home"]), 3000);
      // this.router.navigate(["/home"]);
    }
    this.students = STUDENTS;
  }
}
