import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "dataFilter"
})
export class DataFilterPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    console.log(value, args[0]);
    let myval: any = [];
    myval = value;
    return myval.filter(v => v.name.includes(args[0]));
    // return value;
  }
}
