import { DataService } from "./../services/data.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  constructor(private data: DataService) {}

  users: any;
  filter: string;
  ngOnInit() {
    this.filter = "";
    this.data.getAllUsers().subscribe(data => {
      this.users = data;
    });
  }
  postData() {
    let data = {
      title: "foo",
      body: "bar",
      userId: 1
    };
    this.data.insertPost(data).subscribe(data => console.log(data));
  }
}
