import { ThreeComponent } from "./three/three.component";
import { TwoComponent } from "./two/two.component";
import { OneComponent } from "./one/one.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", component: OneComponent },
  { path: "two", component: TwoComponent },
  { path: "three", component: ThreeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LazyRoutingModule {}
