export const STUDENTS = [
  {
    name: "krishna",
    email: "krishna@krsna.com",
    marks: 78
  },
  {
    name: "john",
    email: "john@krsna.com",
    marks: 88
  },
  {
    name: "tim",
    email: "tim@krsna.com",
    marks: 58
  },
  {
    name: "kelly",
    email: "kelly@krsna.com",
    marks: 90
  }
];
