export const DATA = {
  products: [
    {
      id: 1,
      title: "Pencil",
      description: "A good one",
      price: 30,
      discount: 5,
      isNew: false,
      img:
        "https://images-na.ssl-images-amazon.com/images/I/61qd5YbMRIL._SX466_.jpg"
    },
    {
      id: 2,
      title: "Pen",
      description: "A good one",
      price: 70,
      isNew: false,
      discount: 8,
      img: "http://pngimg.com/uploads/pen/pen_PNG7393.png"
    },
    {
      id: 3,
      title: "School Bag",
      description: "A good one",
      price: 670,
      isNew: true,
      discount: 10,
      img:
        "http://pluspng.com/img-png/png-school-bag-school-bag-png-transparent-image-500.png"
    },
    {
      id: 4,
      title: "Note Book",
      description: "A good one",
      price: 100,
      isNew: true,
      discount: 10,
      img:
        "https://img.pngio.com/notebook-notebook-clipart-book-png-image-and-clipart-for-free-notebook-clipart-png-650_650.png"
    }
  ],
  title: "My Ecommerce Store",
  facebook: "https://facebook.com",
  twitter: "https://twitter.com",
  users: [
    {
      id: 1,
      name: "krishna",
      email: "krishna@krishna.com"
    },
    {
      id: 2,
      name: "john",
      email: "john@krishna.com"
    }
  ],
  countries: ["India", "US", "UK", "Australia"]
};
